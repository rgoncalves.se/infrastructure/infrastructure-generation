#!/usr/bin/env python3

"""
    ____      ____                                                 __  _           
   /  _/___  / __/________ _     ____ ____  ____  ___  _________ _/ /_(_)___  ____ 
   / // __ \/ /_/ ___/ __ `/    / __ `/ _ \/ __ \/ _ \/ ___/ __ `/ __/ / __ \/ __ \
 _/ // / / / __/ /  / /_/ /    / /_/ /  __/ / / /  __/ /  / /_/ / /_/ / /_/ / / / /
/___/_/ /_/_/ /_/   \__,_(_)   \__, /\___/_/ /_/\___/_/   \__,_/\__/_/\____/_/ /_/ 
                              /____/

 ~~~<rgoncalves.se>
"""

import setuptools

try:
	import infra_generation
	from infra_generation import settings
except ImportError:
	print("error: infra_generation loading")

LONG_DESC = open("README.md").read()
VERSION = settings.__version__
DOWNLOAD = "https://gitlab.com/rgoncalves.se/infrastructure/infrastructure-generation/-/archive/%s/infrastructure-generation-%s.tar.gz" % (VERSION, VERSION)

setuptools.setup(
	name = "infra_generation",
	version = VERSION,
	author = settings.__author__,
	author_email = settings.__email__,
	description = "Generate infrastructure config files and graph",
	long_description_content_type = "text/markdown",
	long_description = LONG_DESC,
	keywords = "infrastructure generation infra_generation",
	license = "GPL",
	url = "https://gitlab.com/rgoncalves.se/infrastructure/infrastructure-generation",
	download_url = DOWNLOAD,
	classifiers = [
		"License  :: GPL",
		"Operating System :: POSIX :: Linux",
		"Programming Language :: Python :: 3.5",
		"Programming Language :: Python :: 3.6",
	],
	packages = ["infra_generation"],
	entry_points = {"console_scripts": ["wal=ifnra_generation.__main__:main"]},
	python_requires = ">=3.5",
	include_package_data=True,
	zip_safe=False
)
