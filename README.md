## Introduction

This repository is originally intended to be used as a submodule.

![](example.jpg)

> infrastructure example, created with built-in graph generator

## Install

- For submodule install, i.e. inside infrastructure-ansible :
```bash
git submodule add git@gitlab.com:/rgoncalves.se/infrastructure/infrastructure-generation
```

- For standalone install (you need to edit the env settings) :
```bash
git clone git@gitlab.com:/rgoncalves.se/infrastructure/infrastructure-generation
```

Install python dependencies :
```
pip install -r requirements.txt
```

## Run

```bash
python3 -m infra_generation --env ENVFILE --webapp
python3 -m infra_generation --env ENVFILE --all
python3 -m infra_generation --env ENVFILE --help
```

## Extends

All templates for config files are generated using the jinja2 backend.

## Todo

- [X] webapp frontend allowing dynamic and interactive visualization
- [ ] use a lightweight database backend for host management
- [ ] webapp frontend for creating and editing host, linked to a database
- [X] implement pip packages and allows more command line arguments
