#!/usr/bin/env python3

"""
    ____      ____                                                 __  _
   /  _/___  / __/________ _     ____ ____  ____  ___  _________ _/ /_(_)___  ____
   / // __ \/ /_/ ___/ __ `/    / __ `/ _ \/ __ \/ _ \/ ___/ __ `/ __/ / __ \/ __ \
 _/ // / / / __/ /  / /_/ /    / /_/ /  __/ / / /  __/ /  / /_/ / /_/ / /_/ / / / /
/___/_/ /_/_/ /_/   \__,_(_)   \__, /\___/_/ /_/\___/_/   \__,_/\__/_/\____/_/ /_/
                              /____/

 ~~~<rgoncalves.se>
"""


import os
import argparse
import logging as log
import pathlib

import numpy as np
from dotenv import load_dotenv

from . import diagram
from . import util

def load_env_file(**kwargs):
	"""Creates gloval variables from env file config."""

	# load kwargs file path env
	if "file" in kwargs:
		log.warning(f"Overriding env file to { kwargs.get('file') } ")
		load_dotenv(dotenv_path = pathlib.Path("./" + kwargs.get("file")), override = True)
	# load default env
	else:
		print(load_dotenv())

	global CONFIG_YAML
	global CONFIG_JSON
	global ANSIBLE_INVENTORY_TEMPLATE
	global ANSIBLE_INVENTORY
	global ANSIBLE_HOST_TEMPLATE
	global ANSIBLE_HOST_DIR
	global DOCS_INVENTORY_TEMPLATE
	global DOCS_INVENTORY
	global SSH_CONFIG_TEMPLATE
	global SSH_CONFIG_DIR
	global WG_DC_TEMPLATE
	global WG_DC
	global WG_CLIENT_TEMPLATE
	global WG_CLIENT_DIR
	global DIAGRAM_OUTPUT

	try:
		CONFIG_YAML = os.getenv("CONFIG_YAML")
		CONFIG_JSON = os.getenv("CONFIG_JSON")
		ANSIBLE_INVENTORY_TEMPLATE = os.getenv("ANSIBLE_INVENTORY_TEMPLATE")
		ANSIBLE_INVENTORY = os.getenv("ANSIBLE_INVENTORY")
		ANSIBLE_HOST_TEMPLATE = os.getenv("ANSIBLE_HOST_TEMPLATE")
		ANSIBLE_HOST_DIR = os.getenv("ANSIBLE_HOST_DIR")
		DOCS_INVENTORY_TEMPLATE = os.getenv("DOCS_INVENTORY_TEMPLATE")
		DOCS_INVENTORY = os.getenv("DOCS_INVENTORY")
		SSH_CONFIG_TEMPLATE = os.getenv("SSH_CONFIG_TEMPLATE")
		SSH_CONFIG_DIR = os.getenv("SSH_CONFIG_DIR")
		WG_DC_TEMPLATE = os.getenv("WG_DC_TEMPLATE")
		WG_DC = os.getenv("WG_DC")
		WG_CLIENT_TEMPLATE = os.getenv("WG_CLIENT_TEMPLATE")
		WG_CLIENT_DIR = os.getenv("WG_CLIENT_DIR")
		DIAGRAM_OUTPUT = os.getenv("DIAGRAM_OUTPUT")
	except NameError:
		log.warning("Env file not loaded properly")


def get_args():
	"""Get program arguments from command line."""

	description = "Infrastructure generation."
	arg = argparse.ArgumentParser(description = description)

	arg.add_argument("--process-json", action = "store_true",
					help="Convert the YAML config to a JSON config.")

	arg.add_argument("--process-ansible", action = "store_true",
					help = "Write ansible inventory file.")

	arg.add_argument("--process-docs", action = "store_true",
					help = "Write documentation devices list.")

	arg.add_argument("--process-ssh-config", action = "store_true",
					help = "Write ssh config for inbound/outbound connections.")

	arg.add_argument("--process-wireguard", action = "store_true",
					help = "Write wireguard config for node and clients.")

	arg.add_argument("--process-all", action = "store_true",
					help = "Execute all tasks.")

	arg.add_argument("--webapp", action = "store_true",
					help = "Serve the web application for visualization.")

	arg.add_argument("--env-file", const = "env_file", nargs ="?")

	return arg


def parse_args(parser):
	"""Parse arguments and execute sub-functions accordings to inputs."""

	args = parser.parse_args()

	# Load alternative env file
	if(args.env_file):
		load_env_file(file = args.env_file)
	else:
		load_env_file()

	# Create json file
	if(args.process_json or args.process_all):
		util.convert_yaml_json(CONFIG_YAML, CONFIG_JSON)

	# Create ansible inventoru
	if(args.process_ansible or args.process_all):
		util.process_full_template(
			ANSIBLE_INVENTORY_TEMPLATE,
			CONFIG_YAML,
			ANSIBLE_INVENTORY
		)
		util.process_host_config(
			ANSIBLE_HOST_TEMPLATE,
			util.get_data(CONFIG_YAML),
			"",
			"servers",
			ANSIBLE_HOST_DIR,
			"ini"
		)
		util.process_host_config(
			ANSIBLE_HOST_TEMPLATE,
			util.get_data(CONFIG_YAML),
			"",
			"clients",
			ANSIBLE_HOST_DIR,
			"ini"
		)
		util.process_host_config(
			ANSIBLE_HOST_TEMPLATE,
			util.get_data(CONFIG_YAML),
			"",
			"components",
			ANSIBLE_HOST_DIR,
			"ini"
		)

	# Create documentation markdown pages
	if(args.process_docs or args.process_all):
		util.process_full_template(
			DOCS_INVENTORY_TEMPLATE,
			CONFIG_YAML,
			DOCS_INVENTORY
		)

	# Create ssh configs
	if(args.process_ssh_config or args.process_all):
		util.process_types_config(
			SSH_CONFIG_TEMPLATE,
			util.get_data(CONFIG_YAML),
			"ssh_ansible_infrastructure_",
			"servers",
			SSH_CONFIG_DIR,
			""
		)

	if(args.process_wireguard or args.process_all):
		util.process_full_template(
			WG_DC_TEMPLATE,
			CONFIG_YAML,
			WG_DC
		)
		util.process_host_config(
			WG_CLIENT_TEMPLATE,
			util.get_data(CONFIG_YAML),
			"",
			"servers",
			WG_CLIENT_DIR,
			"conf.origin"
		)
		util.process_host_config(
			WG_CLIENT_TEMPLATE,
			util.get_data(CONFIG_YAML),
			"",
			"clients",
			WG_CLIENT_DIR,
			"conf.origin"
		)
		util.process_host_config(
			WG_CLIENT_TEMPLATE,
			util.get_data(CONFIG_YAML),
			"",
			"components",
			WG_CLIENT_DIR,
			"conf.origin"
		)

	# Draw network diagram
	if(args.webapp):
		diagram.serve_webapp(util.get_data(CONFIG_YAML))

# Main program
def main():
	"""Main entry for infrastructure-generation/generation.py ."""

	parser = get_args()
	parse_args(parser)

if(__name__ == "__main__"):
	main()
