"""
    ____      ____                                                 __  _
   /  _/___  / __/________ _     ____ ____  ____  ___  _________ _/ /_(_)___  ____
   / // __ \/ /_/ ___/ __ `/    / __ `/ _ \/ __ \/ _ \/ ___/ __ `/ __/ / __ \/ __ \
 _/ // / / / __/ /  / /_/ /    / /_/ /  __/ / / /  __/ /  / /_/ / /_/ / /_/ / / / /
/___/_/ /_/_/ /_/   \__,_(_)   \__, /\___/_/ /_/\___/_/   \__,_/\__/_/\____/_/ /_/
                              /____/

 ~~~<rgoncalves.se>
"""

import yaml
import json
from jinja2 import Environment, FileSystemLoader, Template

def get_jinja_env():
	"""Get jinja environment for template manipulation."""

	env = Environment(loader = FileSystemLoader(searchpath = "./"))
	env.trim_blocks = True
	return env


def get_rendered_template(template, data):
	"""Get rendered jinjia template."""

	return get_jinja_env().get_template(template).render(data)


def get_data(data):
	"""Get infrastructure data loaded from yaml root file."""

	return yaml.safe_load(open(data, "r"))


def get_filename(output, prefix, name, filetype):
	"""Get filename associed with templates/filetypes."""

	output = output + "/" + prefix + name
	if len(filetype) > 0:
		output += "." + str(filetype)
	return str(output)


def convert_yaml_json(config_yaml, config_json):
	"""Save yaml config to json config."""

	with open(config_yaml, "r") as config_yaml, open(config_json, "w") as config_json:
		json.dump(yaml.safe_load(config_yaml), config_json)


def process_full_template(template, data, output):
	# Load data
	data = get_data(data)
	# Save config file
	with open(output, "w") as output:
		output.write(get_rendered_template(template, data))


def process_host_config(template, data, prefix, key, output, filetype):
	"""Create config file from jinja template per hosts."""

	# We want to save each hosts in its own config file
	for host in data[key]:
		# Convert template and save file in output directory
		with open(get_filename(output, prefix, host["name"], filetype), "w") as output_file:
			output_file.write("# " + host["name"] + "\n")
			output_file.write(get_rendered_template(template, host))


def process_types_config(template, data, prefix, key, output, filetype):
	"""Create config file from jinja template per host types.
	(servers, clients, components, ...)"""

	with open(get_filename(output, prefix, key, filetype), "w") as output_file:
		for host in data[key]:
			output_file.write(get_rendered_template(template, host))


