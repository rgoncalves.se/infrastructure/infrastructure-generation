"""
    ____      ____                                                 __  _
   /  _/___  / __/________ _     ____ ____  ____  ___  _________ _/ /_(_)___  ____
   / // __ \/ /_/ ___/ __ `/    / __ `/ _ \/ __ \/ _ \/ ___/ __ `/ __/ / __ \/ __ \
 _/ // / / / __/ /  / /_/ /    / /_/ /  __/ / / /  __/ /  / /_/ / /_/ / /_/ / / / /
/___/_/ /_/_/ /_/   \__,_(_)   \__, /\___/_/ /_/\___/_/   \__,_/\__/_/\____/_/ /_/
                              /____/

 ~~~<rgoncalves.se>
"""


import logging

import dash

from dash.dependencies import Input, Output

import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
import networkx as nx
import plotly.graph_objects as go
import plotly.express as px
import json


logging.basicConfig(level = logging.DEBUG)


def create_nodes(diagram, data):
	"""Create nodes for graph."""

	for host_category in data:
		for host_index in range(len(data[host_category])):
			host = data[host_category][host_index]
			diagram.add_node(host["name"])
			diagram.nodes[host["name"]].update(host)


def create_edges(diagram):
	"""Create edges between connected nodes, allowing the right layout."""

	for index, node in diagram.nodes(data = True):
		try:
			diagram.add_edge(node["name"], node["path"][0])
			for bounce_index in range(1, len(node["path"])):
				diagram.add_edge(node["path"][bounce_index - 1], node["path"][bounce_index], width = 10.0)
		except KeyError:
			logging.warning("KeyError")


def create_coordinates(diagram):
	"""Return nodes coordinates thanks to networkx layout generation."""

	return nx.nx_agraph.graphviz_layout(
		diagram,
		prog = "dot"
	)


def get_color_from_role(hostename):
	"""Return a marker according to the host role."""

	host_role = hostename.split("-")

	# Abstract component
	if len(host_role) == 1:
		return "cyan"

	# Client component
	if len(host_role) == 2:
		return "gold"

	# Server component
	if len(host_role) == 3 :
		return "GreenYellow"

	# UNNKOWN component
	return "black"


def create_information_from_json(text):
	return text


def convert_diagram_to_go(diagram, coords):
	"""Create 'Graph Objects' diagram frontend."""

	node_x = []
	node_y = []
	node_text = []
	edge_x = []
	edge_y = []
	node_color = []


	for index, pos in coords.items():
		node_x.append(pos[0])
		logging.debug(f"for node {index} : posX = {pos[0]}")
		node_y.append(pos[1])
		logging.debug(f"for node {index} : posY = {pos[1]}")
		node_text.append(index)

	buffer = ""
	# iterate through each device
	for name, node in diagram.nodes(data = True):
		count = 0
		logging.debug(f" -  iterating through device {name}")
		node_color.append(get_color_from_role(name))

		try:
			# iterate for each bounced host during internet connection
			for bounce in node["path"]:

				# deal with first devices
				if count == 0:
					buffer = name
					logging.debug(f"   - hit {bounce}")
				edge_x.append(coords[buffer][0])
				edge_x.append(coords[bounce][0])
				edge_x.append(None)
				edge_y.append(coords[buffer][1])
				edge_y.append(coords[bounce][1])
				edge_y.append(None)
				buffer = bounce
				count += 1
		except KeyError:
			logging.error(f"KeyError with {node}")
			count += 1


	node_trace = go.Scatter(
		text = node_text,
		x = node_x,
		y = node_y,
		mode = "markers",
		hoverinfo = "text",
		marker = dict(
			showscale = True,
			colorscale = "YlGnBu",
			reversescale = True,
			color = node_color,
			size = 10,
			colorbar = dict(
				thickness = 15,
				title = "Node Connections",
				xanchor = "left",
				titleside = "right"
			),
			line_width = 2
		)
	)

	edge_trace = go.Scatter(
		x = edge_x,
		y = edge_y,
		line = dict(
			width = 2,
			color = "#555555",
			#shape = "vhv"
		),
		mode = "lines"
	)

	figure = go.Figure(
		data = [
			edge_trace,
			node_trace
		],
		layout = go.Layout(
			titlefont_size = 16,
			showlegend = True,
			hovermode = "closest",
			hoverdistance = -1,
			margin = dict(
				b = 20,
				l= 5,
				r = 5,
				t = 40
			),
			annotations = [
				dict(
					showarrow=False,
					xref = "paper",
					yref = "paper",
					x = .005,
					y = -.002
				)
			],
			xaxis = dict(
				showgrid = False,
				zeroline = False,
				showticklabels = False
			),
			yaxis = dict(
				showgrid = False,
				zeroline = False,
				showticklabels = False
			)
		)
	)

	# remove unnecessary legends and colormap
	figure.update_layout(hovermode = "closest")
	figure.update_traces(showlegend = False)
	figure.update(layout_showlegend = False)
	figure.update_traces(marker_showscale = False)
	figure.update(layout_coloraxis_showscale = False)
	# disabling hoverdistance threshold

	return figure


def show_diagram(figure):
	"""Render the network diagram in a web page."""
	app.title = "network viewer"
	app.layout = dbc.Container(
		className = "p-0",
		children = [
			dbc.NavbarSimple(
				brand = "network visualizer",
				brand_href = "#",
				color = "primary",
				dark = True,
				className = "shadow"
			),
			html.Br(),
			dbc.Container([
				dbc.CardDeck([
					dbc.Card(
						[
							dbc.CardHeader("Network diagram"),
							dbc.CardBody([
								dcc.Graph(id = "network-diagram", figure = figure)
							])
						], className = "shadow-sm"
					),
					dbc.Card(
						[
							dbc.CardHeader("Network informations"),
							dbc.CardBody([
								html.Pre(id = "network-informations")
							])
						], className = "shadow-sm"
					)
				])
			], fluid = True),
			html.Br(),
			dbc.Container([
				dbc.CardDeck([
					dbc.Card(
						[
							dbc.CardHeader("Controls"),
							dbc.CardBody(":/")
						], className = "shadow-sm"
					)
				])
			], fluid = True)
		],
		fluid = True
	)
	app.run_server(debug = True)


def get_hovered_hostname(hover_data):
	"""Return the hovered hostname"""

	try:
		output = hover_data["points"][0]["text"]
	except TypeError:
		output = "Please select a network component on the diagram"

	return output


def get_host_data(hostname):
	"""Return all network data related to a particular host"""

	for host_type in yaml_data:
		for host_data in yaml_data[host_type]:
			if hostname == host_data["name"]:
				return host_data
	return("Hover a node to get informations.")


def get_information_table(data):
	"""Create a dash-bootstrap table object with network informations."""

	table_header = [
		html.Thead(
			html.Tr([
				html.Th("Key"),
				html.Th("Value")
			])
		)
	]

	rows = []
	try:
		for line in data.items():
			rows.append(
				html.Tr([
					html.Td(str(line[0])),
					html.Td(str(line[1]))
				])
			)
	except AttributeError:
		pass

	if len(rows) == 0:
		return dbc.Toast(
			[
				html.P("Select a network component")
			],
			header = "Help"
		)


	return dbc.Table(
		table_header + [html.Tbody(rows)],
		bordered = False,
		hover = True,
		responsive = True
	)


def serve_webapp(data):
	"""Server the web application."""

	global yaml_data
	yaml_data = data

	diagram = nx.OrderedDiGraph()
	create_nodes(diagram, data)
	create_edges(diagram)

	coords = create_coordinates(diagram)
	fig = convert_diagram_to_go(diagram, coords)
	show_diagram(fig)


# Empty object, allowing generation through function from above
app = dash.Dash(
	__name__,
	external_stylesheets = [
		dbc.themes.BOOTSTRAP
	]
)


# Diagram hover event
@app.callback(
	Output("network-informations", "children"),
	[
		Input("network-diagram", "clickData")
	]
)
def disp_hover_data(hover_data):
	"""Display data on hover"""

	name = get_hovered_hostname(hover_data)
	text = get_host_data(name)
	table = get_information_table(text)
	return table
